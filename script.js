function sendReq(onloadHandler) {
    let req = new XMLHttpRequest();
    req.open("GET", "http://exam-2020-1-api.std-900.ist.mospolytech.ru/api/data1");
    req.responseType = 'json';
    req.onload = onloadHandler;
    req.send();
}

let allResultsArr = [];

let setsObj = {menuMIP0: 0, menuMIP1: 0, menuMIP2: 0, menuMIP3: 0, menuMIP4: 0,
                menuMIP5: 0, menuMIP6: 0, menuMIP7: 0, menuMIP8: 0, menuMIP9: 0}; // for each sets prices + info
let setsObjSum = {menuMIP0: 0, menuMIP1: 0, menuMIP2: 0, menuMIP3: 0, menuMIP4: 0,
                    menuMIP5: 0, menuMIP6: 0, menuMIP7: 0, menuMIP8: 0, menuMIP9: 0}; // for each set sum
let menuSum = 0; // for end sum
let oldMenuSum = 0; // just for design
let special1Changed = false;
let special2Changed = false;
let socialDiscount = 0;
let rest; // for restaurant info
let jnArr = []; // for downloading menu from .json



// Restaurants searching functions

function cleanSearchResults () {
    while (document.querySelector("#restaurants > .row").nextElementSibling != null){ // clean table after previous search
        document.querySelector("#restaurants > .row").nextElementSibling.remove();
    }
    while (document.getElementById("searchPagesBtns").firstChild != null){ // clean btns after previous search
        document.getElementById("searchPagesBtns").firstChild.remove();
    }
}

function searchParamBuilder (records) {
    let admAreaElem = document.getElementById("admArea"),
    districtElem = document.getElementById("district"),
    typeObjectElem = document.getElementById("typeObject"),
    socialPrivilegesElem = document.getElementById("socialPrivileges");
    let admAreaArr = [], districtArr = [], typeObjectArr = [], socialPrivilegesArr = [];
    for (let record of records){
        if (record.admArea !== undefined && record.district !== undefined &&
            record.typeObject !== undefined && record.socialPrivileges !== undefined &&
            record.admArea !== null && record.district !== null &&
            record.typeObject !== null && record.socialPrivileges !== null &&
            record.address !== undefined && record.address !== null && // to show it properly in results
            record.rate !== undefined && record.rate !== null){ // to show it properly in results
            if (!admAreaArr.includes(record.admArea)){
                let elem = document.createElement('option');
                elem.textContent = record.admArea;
                elem.value = record.admArea;
                admAreaElem.append(elem);
                admAreaArr.push(record.admArea);
            }
            if (!districtArr.includes(record.district)){
                let elem = document.createElement('option');
                elem.textContent = record.district;
                elem.value = record.district;
                districtElem.append(elem);
                districtArr.push(record.district);
            }
            if (!typeObjectArr.includes(record.typeObject)){
                let elem = document.createElement('option');
                elem.textContent = record.typeObject;
                elem.value = record.typeObject;
                typeObjectElem.append(elem);
                typeObjectArr.push(record.typeObject);
            }
            if (!socialPrivilegesArr.includes(record.socialPrivileges)){
                let elem = document.createElement('option');
                if (record.socialPrivileges == 0){
                    elem.textContent = "Нет";
                    elem.value = record.socialPrivileges;
                }
                else{
                    elem.textContent = "Есть";
                    elem.value = record.socialPrivileges;
                }
                socialPrivilegesElem.append(elem);
                socialPrivilegesArr.push(record.socialPrivileges);
            }
        }
    }
}

function searchPagesBtnsRenderer (text, pages, current) {
    let elem = document.getElementById("searchPagesBtns");
    let li = document.createElement("li");
    let btn = document.createElement("button");
    li.className = "page-item css-shadow-gray";
    btn.textContent = text;
    text = (text == "В конец") ? pages : (text == "В начало") ? 1 : text;
    btn.value = text;
    btn.className = (text == current) ? "page-link border-dark bg-secondary text-light" : "page-link border-dark css-bg-black text-light";
    btn.setAttribute('onclick','searchResultsPagesRenderer(parseInt(this.value, 10))');
    li.append(btn);
    elem.append(li);

    btn.onmouseover = function(event) {
        if (text != current) event.target.className = "page-link border-dark bg-light text-dark";
    };      
    btn.onmouseout = function(event) {
        if (text != current) event.target.className = "page-link border-dark css-bg-black text-light";
    };
}

function searchResultsPagesRenderer (pageNum){
    cleanSearchResults ();
    let resAmount = allResultsArr.length,
    countBy = 10, // how many results for one page
    pagesAmount = Math.ceil(resAmount/countBy);
    let pageArr = [], j = 0;
    for (let i = (pageNum - 1)*countBy; i < ((pageNum)*countBy) && i < resAmount; i++){ // rewrite results only on pageNum
        pageArr[j] = allResultsArr[i];
        j++;
    }
    let elem = document.getElementById("restaurants");
    pageArr.forEach(function(res){ // render each search-result
        let body = document.createElement('div'),
        colName = document.createElement('div'),
        colType = document.createElement('div'),
        colAddr = document.createElement('div'),
        colBtnBody = document.createElement('div'),
        btn = document.createElement('button');

        body.className = "row border-top border-dark d-flex align-items-center";
        colName.className = "col-3";
        colType.className = "col-2 col-md-3 col-sm-2";
        colAddr.className = "col-3 col-md-4 col-sm-3";
        colBtnBody.className = "col-4 col-md-2 col-sm-4";
        btn.className = "btn btn-danger btn-sm btn-block my-2 font-weight-bold";
        btn.type = "button";
        btn.value = res.id;
        btn.textContent = "Выбрать";
        btn.setAttribute('onclick','menuBuilder(parseInt(this.value, 10))');

        //colName.textContent = `${res.rate} | ${res.name}`;
        colName.textContent = res.name;
        colType.textContent = res.typeObject;
        colAddr.textContent = res.address;

        colBtnBody.append(btn);
        body.append(colName);
        body.append(colType); 
        body.append(colAddr); 
        body.append(colBtnBody); 
        elem.append(body);
    });

    if (pagesAmount == 1) return; // if only one page than no need to render btns
    
    elem = document.getElementById("searchPagesBtns");
    if (pagesAmount <= 3){
        for (let i = 1; i <= pagesAmount; i++){
            searchPagesBtnsRenderer(i, pagesAmount, pageNum);
        }
    } else if (pageNum >= pagesAmount-1){
        searchPagesBtnsRenderer("В начало", 1, 0);
        for (let i = (pagesAmount - 2); i <= pagesAmount; i++){
            searchPagesBtnsRenderer(i, pagesAmount, pageNum);
        }
    } else if (pageNum <= 2 && pagesAmount >= 4){
        for (let i = 1; i <= 3; i++){
            searchPagesBtnsRenderer(i, pagesAmount, pageNum);
        }
        searchPagesBtnsRenderer("В конец", pagesAmount, 0);
    } else if (pageNum >= 3 && pageNum <= (pagesAmount - 2)){
        searchPagesBtnsRenderer("В начало", 1, 0);
        for (let i = (pageNum - 1); i <= (pageNum + 1); i++){
            searchPagesBtnsRenderer(i, pagesAmount, pageNum);
        }
        searchPagesBtnsRenderer("В конец", pagesAmount, 0);
    }
}

function searchResultsBuilder (records) {
    allResultsArr = [];
    cleanSearchResults();
    let admAreaVal = document.getElementById("admArea").value,
    districtVal = document.getElementById("district").value,
    typeObjectVal = document.getElementById("typeObject").value,
    socialPrivilegesVal = document.getElementById("socialPrivileges").value;
    let findFlag = false; // find it or not?
    if (admAreaVal == "Не выбрано" || districtVal == "Не выбрано" || typeObjectVal == "Не выбрано" || socialPrivilegesVal == "Не выбрано"){
        searchInfoParent = document.getElementById("searchInfo");
        if (searchInfoParent === null){
            let searchInfoBody = document.createElement('div');
            searchInfoBody.className = "row border-top border-dark bg-danger";
            searchInfoBody.id = "searchInfo";

            let searchInfoText = document.createElement('div');
            searchInfoText.className = "col-12 text-center py-2";
            searchInfoText.textContent = 'Задайте параметры поиска и нажмите "Найти".';

            searchInfoBody.append(searchInfoText);
            document.querySelector("#restaurants").insertBefore(searchInfoBody, document.querySelector("#restaurants > .row").nextSibling);
        }
        else {
            searchInfoParent.className = "row border-top border-dark bg-danger";
        }
        return;
    }
    else {
        if (document.getElementById("searchInfo") !== null) document.getElementById("searchInfo").remove();
        for (let record of records){
            if (record.address === null || record.address === undefined || record.rate === null || record.rate === undefined){ // to show it properly in results
                continue;
            }
            if (admAreaVal != "Любое" && record.admArea != admAreaVal ||
            districtVal != "Любое" && record.district != districtVal ||
            typeObjectVal != "Любое" && record.typeObject != typeObjectVal ||
            socialPrivilegesVal != "Любое" && record.socialPrivileges != socialPrivilegesVal){
                continue;
            }
            findFlag = true;
            allResultsArr.push(record);
        }
    }
    if (findFlag == false){
        searchInfoParent = document.getElementById("searchInfo");
        if (searchInfoParent === null){
            let searchInfoBody = document.createElement('div');
            searchInfoBody.className = "row border-top border-dark bg-primary";
            searchInfoBody.id = "searchInfo";

            let searchInfoText = document.createElement('div');
            searchInfoText.className = "col-12 text-center py-2";
            searchInfoText.textContent = 'Ничего не найдено.';

            searchInfoBody.append(searchInfoText);
            document.querySelector("#restaurants").insertBefore(searchInfoBody, document.querySelector("#restaurants > .row").nextSibling);
        }
        else {
            searchInfoParent.className = "row border-top border-dark bg-primary";
            searchInfoParent.textContent = 'Ничего не найдено.';
        }
    }
    else {
        allResultsArr.sort(function (x, y) { // sorting array of results by rate
            return y.rate - x.rate;
        });
        searchResultsPagesRenderer(1); // rander first page
    }
}

// Menu (food) functions

function cleanMenu () {
    while (document.getElementById("foodMenu").firstChild != null){ // clean menu after previous loading
        document.getElementById("foodMenu").firstChild.remove();
    }
    document.querySelector(`input[name='special-1']`).disabled = false;
    document.querySelector(`input[name='special-1']`).checked = false;
    document.getElementById("special-1_label").className = "col-9 col-md-5 col-sm-6 text-light";
    document.querySelector(`input[name='special-2']`).checked = false;
    document.querySelector(`input[name='special-2']`).disabled = true;
    document.getElementById("special-2_label").className = "col-9 col-md-5 col-sm-6 text-secondary";

    oldMenuSum = 0;
    menuSum = 0;
    document.getElementById("menuSum").textContent = menuSum;
}

function menuRenderer (price, id) {
    inhtml = `<div class="d-flex flex-column justify-content-between col-8 col-md-3 col-sm-8 text-center css-shadow-gray rounded border border-dark px-0 pb-3 mb-3 mx-2">
                    <div class="row d-flex align-items-top">
                        <div class="col-12">
                            <img class="border-bottom border-dark rounded-top w-100 h-auto mb-2" src="media/Menu/${jnArr[0]+1}.jpg" alt="...">
                            <h5 class="px-1"><b>${jnArr[1]}</b></h5>
                            <small class="px-1">${jnArr[2]}</small>
                        </div>                    
                    </div>
                    <div class="row d-flex justify-content-center align-items-center">
                        <div class="col-12 px-0 d-flex justify-content-center">
                            <h3>${price} ₽</h3>
                        </div>
                        <div class="col-2 px-0 d-flex justify-content-center">
                            <button class="btn btn-primary btn-sm btn-block css-btn-size" type="button" name="${id}" value="-1" onclick="menuPlusMinus(parseInt(this.value, 10), this.name)">
                                <i class="fa fa-minus fa-lg"></i>
                            </button>
                        </div>
                        <div class="col-4 px-1 d-flex justify-content-center">
                            <input class="form-control" type="text" size="1" name="${id}" onchange="inputChanged(this.name)">
                        </div>
                        <div class="col-2 px-0 d-flex justify-content-center">
                            <button class="btn btn-primary btn-sm btn-block css-btn-size" type="button" name="${id}" value="1" onclick="menuPlusMinus(parseInt(this.value, 10), this.name)">
                                <i class="fa fa-plus fa-lg"></i>
                            </button>
                        </div>
                    </div>
                </div>`
    document.getElementById("foodMenu").insertAdjacentHTML('beforeend', inhtml);
    document.querySelector(`input[name='${id}']`).value = 0;
}

function inputChanged (name){
    let input = document.querySelector(`input[name='${name}']`);
    menuPlusMinus(0, input.name);
    menuSum = 0;
    oldMenuSum = 0;
    for(let i in setsObj) {
        if (i == input.name){
            setsObjSum[i] = parseInt(setsObj[i], 10) * parseInt(input.value, 10);
        }
        menuSum += setsObjSum[i];
    }
    sp1 = document.querySelector(`input[name='special-1']`).checked;
    sp2 = document.querySelector(`input[name='special-2']`).checked;
    oldMenuSum = menuSum;
    if (sp1 == true && sp2 == false) {
        menuSum = Math.ceil((menuSum/200)*160);
    } else if (sp1  == true && sp2 == true) {
        menuSum = Math.ceil((Math.ceil(menuSum/200)*160)/100)*(100-socialDiscount);
    } else if (sp1  == false && sp2 == true) {
        menuSum = Math.ceil((menuSum/100)*(100-socialDiscount));
    } else {
        document.getElementById("menuSum").textContent = menuSum;
        if (menuSum > 0){
            document.getElementById("makeOrderBtn").disabled = false;
        } else {
            document.getElementById("makeOrderBtn").disabled = true;
        }
        return;
    }
    menuSum = (oldMenuSum != 0) ? menuSum : 0;
    document.getElementById("menuSum").innerHTML = `<span class="text-secondary" style="text-decoration: line-through">${oldMenuSum}</span> <b>${menuSum}</b>`;
    if (menuSum > 0){
        document.getElementById("makeOrderBtn").disabled = false;
    } else {
        document.getElementById("makeOrderBtn").disabled = true;
    }
}

function menuPlusMinus (mp, name) {
    let input = document.querySelector(`input[name='${name}']`);
    special1 = document.querySelector(`input[name='special-1']`).checked;
    mp = (special1 == false) ? mp : mp*2;
    
    let ok = true;
    if (parseInt(input.value[0]) == 0 && input.value.length >= 2) ok = false;
    for (let i = 0; i < input.value.length && ok == true; i++){
        if (isNaN(parseInt(input.value[i]))){
            ok = false;
            break;
        }
    }
    if (ok == false){
        input.value = 0;
        return;
    }
    let inVal = parseInt(input.value, 10);
    input.value = (special1 == true && special1Changed == false && ((inVal%2) != 0)) ? (inVal+1) : inVal; // keep at count by 2, while special1 is checked
    inVal = parseInt(input.value, 10);
    let val = inVal + mp;
    if (special1 == false && special1Changed == false){
        inVal = (val >= 0 && val <= 99) ? val : inVal;
        inVal = (inVal < 0) ? 0 : (inVal > 99) ? 99 : inVal;
        input.value = inVal;
    } else if (special1 == false && special1Changed == true){
        inVal /= 2;
        special1Changed = false;
        inVal = (inVal < 0) ? 0 : (inVal > 99) ? 99 : inVal;
        input.value = inVal;
    } else if (special1 == true && special1Changed == false){
        inVal = (val >= 0 && val <= 98) ? val : inVal;
        inVal = (inVal < 0) ? 0 : (inVal > 98) ? 98 : inVal;
        input.value = inVal;
    } else {
        inVal *= 2;
        special1Changed = false;
        inVal = (inVal < 0) ? 0 : (inVal > 98) ? 98 : inVal;
        input.value = inVal;
    }
    if (mp != 0) inputChanged(name);
}

function menuBuilder (restId) {
    cleanMenu();

    for (s in setsObj){
        setsObj[s] = 0;
    }
    for (s in setsObjSum){
        setsObjSum[s] = 0;
    }

    rest = allResultsArr.find(function (r) {return r.id == restId});
    if (rest.socialPrivileges == 1) {
        document.querySelector(`input[name='special-2']`).disabled = false;
        document.querySelector(`input[name='special-2']`).checked = false;
        document.getElementById("special-2_label").className = "col-9 col-md-5 col-sm-6 text-light";
        socialDiscount = parseInt(rest.socialDiscount, 10);
    }
    if (menuSum > 0){
        document.getElementById("makeOrderBtn").disabled = false;
    } else {
        document.getElementById("makeOrderBtn").disabled = true;
    }
    setsObj = {menuMIP0: rest.set_1, menuMIP1: rest.set_2, menuMIP2: rest.set_3, menuMIP3: rest.set_4, menuMIP4: rest.set_5,
    menuMIP5: rest.set_6, menuMIP6: rest.set_7, menuMIP7: rest.set_8, menuMIP8: rest.set_9, menuMIP9: rest.set_10};

    fetch("media/Menu/menu.json")
        .then(function(response) { return response.json(); })
        .then(function (json){
            for (let i in setsObj){ 
                jnArr[0] = parseInt(i[7],10);
                jnArr[1] = json[`set_${(jnArr[0]+1)}`][0]['title'];
                jnArr[2] = json[`set_${(jnArr[0]+1)}`][0]['desc'];
                menuRenderer(setsObj[i], i);
            }
        });
}

// "specials" function

function specialOnChecked(checkbox){
    if (checkbox.name == "special-1"){
        for(i in setsObj){
            special1Changed = true;
            inputChanged(i);
        }
    } else {
        for(i in setsObj){
            special2Changed = true;
            inputChanged(i);
        }
    };

}

// ordering

function orderInfo(){
    let special1 = "";
    let special2 = "";
    
    if (document.querySelector(`input[name='special-1']`).checked){
        special1 = `<div class="row">
                        <div class="col-6 p-0">
                            В два раза больше:
                        </div>
                        <div class="col-6 p-0 d-flex justify-content-end">
                            <div><b>x2</b> позиций, <b>+60%</b> к цене</div>
                        </div>
                    </div>`;
    };
    if (document.querySelector(`input[name='special-2']`).checked){
        special2 = `<div class="row">
                        <div class="col-6 p-0">
                            Социальная скидка:
                        </div>
                        <div class="col-6 p-0 d-flex justify-content-end">
                            <div><b>-${rest.socialDiscount}%</b> от цены</div>
                        </div>
                    </div>`;
    };

    while (document.querySelector("#modelOrderInfo > #modelPositions").nextElementSibling.id != "modelSpecials"){
        document.querySelector("#modelOrderInfo > #modelPositions").nextElementSibling.remove();
    }
    while (document.querySelector("#modelOrderInfo > #modelSpecials").nextElementSibling.id != "modelPlaceInfo"){
        document.querySelector("#modelOrderInfo > #modelSpecials").nextElementSibling.remove();
    }
    while (document.querySelector("#modelOrderInfo > #modelPlaceInfo").nextElementSibling.id != "modelTransport"){
        document.querySelector("#modelOrderInfo > #modelPlaceInfo").nextElementSibling.remove();
    }
    fetch("media/Menu/menu.json")
    .then(function(response) { return response.json(); })
    .then(function (json){
        for (let i in setsObjSum){ 
            jnArr[0] = parseInt(i[7],10);
            jnArr[1] = json[`set_${(jnArr[0]+1)}`][0]['title'];
            jnArr[2] = json[`set_${(jnArr[0]+1)}`][0]['desc'];
            let inputVal = document.querySelector(`input[name='${i}']`).value;
            if (inputVal == 0) continue;
            let inhtml = `<div class="row border rounded border-dark d-flex align-items-center mb-1">
                                <div class="col-2 p-0">
                                    <img class="border border-dark rounded w-100 h-auto m-1" src="media/Menu/${jnArr[0]+1}.jpg" alt="...">
                                </div>
                                <div class="col-10 col-md-4 col-sm-10">
                                    <h5>${jnArr[1]}</h5>
                                </div>
                                <div class="col-6 col-md-3 col-sm-6 d-flex justify-content-center">
                                    <div>${inputVal}x${setsObj[i]} ₽</div>
                                </div>
                                <div class="col-6 col-md-3 col-sm-6 d-flex justify-content-center">
                                    <div> = <b>${setsObjSum[i]} ₽</b></div>
                                </div>
                            </div>`;
            document.querySelector("#modelOrderInfo > #modelPositions").insertAdjacentHTML('afterend', inhtml);
        }
    });
    if (special1 != special2){ // specials
        document.querySelector("#modelOrderInfo > #modelSpecials").insertAdjacentHTML('afterend', special1+special2 );
    } else {
        let inhtml = `<div class="row">
                        <div class="col-12 p-0">
                            Ни одна опция не выбрана.
                        </div>
                    </div>`;
        document.querySelector("#modelOrderInfo > #modelSpecials").insertAdjacentHTML('afterend', inhtml);   
    }

    // place info
    let inhtml = `<div class="row mb-2">
                        <div class="col-6 p-0">
                            Название:
                        </div>
                        <div class="col-6 p-0">
                            ${rest.name}
                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-6 p-0">
                            Адм. округ:
                        </div>
                        <div class="col-6 p-0">
                            ${rest.admArea}
                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-6 p-0">
                            Район:
                        </div>
                        <div class="col-6 p-0">
                            ${rest.district}
                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-6 p-0">
                            Адрес:
                        </div>
                        <div class="col-6 p-0">
                            <small>${rest.address}</small>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6 p-0">
                            Рейтинг:
                        </div>
                        <div class="col-6 p-0">
                            ${rest.rate}
                        </div>
                    </div>`;

    document.querySelector("#modelOrderInfo > #modelPlaceInfo").insertAdjacentHTML('afterend', inhtml);

    // end price
    if (special1 != special2){
        document.querySelector("#modelEndPrice div:nth-child(2)").innerHTML = `<h4><span class="text-secondary" style="text-decoration: line-through">${oldMenuSum+250} ₽</span> ${menuSum+250} ₽</h4>`;
    } else {
        document.querySelector("#modelEndPrice div:nth-child(2) > h4 ").textContent = `${menuSum+250} ₽`;
    }
}

function orderError(e){
    let err = (e == 1) ? "Неверная зона доставки!" : (e == 2) ? "Неверный адрес!" : "Неверное ФИО!";
    err = `<div class="row alert alert-danger border border-dark" role="alert">
                <div class="col-12">
                    ${err}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Закрыть"><i class="fa fa-times"></i></button>
                </div>
            </div>`
    document.getElementById("modelOrderInfo").insertAdjacentHTML('beforeend', err);
}

function orderChecker(){
    let ok = true;
    let buff = document.getElementById("modelTransport").nextElementSibling.querySelector("div:nth-child(2) > input").value;
    if (parseInt(buff).toString().length !== buff.length){ // parseInt to clean input from letters and after that go back to string to compare result with input and know if it was ok or not
        if (ok == true){
            while (document.querySelector("#modelOrderInfo > .alert[role='alert']")){
                document.querySelector("#modelOrderInfo > .alert[role='alert']").remove();
            }
        }
        orderError(1);
        ok = false;
    }
    buff = document.getElementById("modelTransport").nextElementSibling.nextElementSibling.querySelector("div:nth-child(2) > textarea").value;
    if (buff.length < 1) {
        if (ok == true){
            while (document.querySelector("#modelOrderInfo > .alert[role='alert']")){
                document.querySelector("#modelOrderInfo > .alert[role='alert']").remove();
            }
        }
        orderError(2);
        ok = false;
    }

    buff = document.getElementById("modelTransport").nextElementSibling.nextElementSibling.nextElementSibling.nextElementSibling.querySelector("div:nth-child(2) > input").value;
    if (buff.length < 1){
        if (ok == true){
            while (document.querySelector("#modelOrderInfo > .alert[role='alert']")){
                document.querySelector("#modelOrderInfo > .alert[role='alert']").remove();
            }
        }
        orderError(3);
        ok = false;
    }
    if (ok == false){
        document.getElementById("modelOrderInfo").scrollIntoView({block: "end", behavior: "smooth"});
        return;
    }
    while (document.querySelector("#modelOrderInfo > .alert[role='alert']")){
        document.querySelector("#modelOrderInfo > .alert[role='alert']").remove();
    }
    document.querySelector(".modal-header > button").click();
                    
    let inhtml = `<div id="alertSuccess" class="row alert alert-primary py-2 border border-dark" role="alert">
                    <div class="col-12">
                        Заказ оформлен!
                        <button type="button" class="close" data-dismiss="alert" aria-label="Закрыть"><i class="fa fa-times"></i></button>
                    </div>
                </div>`
    while (document.getElementById("alertSuccess")){
        document.getElementById("alertSuccess").remove();
    }
    document.getElementById("infoMsgsDiv").insertAdjacentHTML('beforeend', inhtml);
    setTimeout(function() {window.scrollTo({top: 0, left: 0, behavior: "smooth"});}, 500);
}

window.onload = function() {

    document.querySelector(`input[name='special-1']`).checked = false;
    document.querySelector(`input[name='special-1']`).disabled = true;
    document.getElementById("special-1_label").className = "col-9 col-md-5 col-sm-6 text-secondary";
    document.querySelector(`input[name='special-2']`).checked = false;
    document.querySelector(`input[name='special-2']`).disabled = true;
    document.getElementById("special-2_label").className = "col-9 col-md-5 col-sm-6 text-secondary";
    document.getElementById("makeOrderBtn").disabled = true;

    sendReq(function () {searchParamBuilder(this.response)});

    document.getElementById("searchBtn").onclick = function () {
        sendReq(function () {searchResultsBuilder(this.response)});
    };
    document.getElementById("makeOrderBtn").onclick = function () { orderInfo(); };
    document.getElementById("confOrderBtn").onclick = function () { orderChecker(); };
}