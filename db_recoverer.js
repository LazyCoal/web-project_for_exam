let host = "http://exam-2020-1-api.std-400.ist.mospolytech.ru/api/data1";
let i=0;
let arr = [];

function sendReq(url, method, onloadHandler, params) {
    console.log(url);
    let req = new XMLHttpRequest();
    req.open(method, url);
    req.responseType = 'json';
    req.onload = onloadHandler;
    req.send(params);
}

function rI(min, max) {
    // случайное число от min до (max+1)
    let rand = min + Math.random() * (max + 1 - min);
    return Math.floor(rand);
}

function add_f (a){
    alert(a);
    alert(a.id);
}

function changed (a){
    i = parseInt(a,10);
    console.log(`i (ch) = ${i}`);
}

window.onload = function() {

    document.getElementById("delete").onclick = function() {
            //166062
            sendReq(host, "GET", function () { 
                arr = [];
                let a = this.response;
                for (q in a){
                    arr.push(a[q].id);
                }
                console.log(`a = ${a}`);
            });
            let timerId = setInterval( function () {
                if (arr[i] >= 999999 || i >= arr.length){
                    clearInterval(timerId);
                    return;
                };
                let newurl = host + `/${arr[i]}`;
                sendReq(newurl, "DELETE", function () {
                let a = this.response;
                console.log(`a = ${a}`);
                i = i+1;
                console.log(`i = ${i}`);
            })}, 1000);
    }
    document.getElementById("add").onclick = function() {
        let obj = new FormData();
        obj.append('address', `адрес ${rI(1,100000)} by LC`)
        obj.append('admArea', `округ ${rI(1,10)} by LC`)
        obj.append('district', `район ${rI(1,10)} by LC`)
        obj.append('isNetObject', rI(0,1))
        obj.append('name', `название ${rI(1,100000)} by LC`)
        obj.append('operatingCompany', `operComp ${rI(1,10)} by LC`)
        obj.append('publicPhone', `8 (800) 555-35-${rI(1,100000)}`)
        obj.append('rate', rI(1,99))
        obj.append('seatsCount', rI(1,500))
        obj.append('set_1', rI(1,1000))
        obj.append('set_2', rI(1,1000))
        obj.append('set_3', rI(1,1000))
        obj.append('set_4', rI(1,1000))
        obj.append('set_5', rI(1,1000))
        obj.append('set_6', rI(1,1000))
        obj.append('set_7', rI(1,1000))
        obj.append('set_8', rI(1,1000))
        obj.append('set_9', rI(1,1000))
        obj.append('set_10', rI(1,1000))
        obj.append('socialDiscount', rI(1,99))
        obj.append('socialPrivileges', rI(0,1))
        obj.append('typeObject', `тип ${rI(1,10)}`);
        sendReq(host, "POST", function () {add_f(this.response)}, obj);
    };
}