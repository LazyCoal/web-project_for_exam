let host = "http://exam-2020-1-api.std-900.ist.mospolytech.ru/api/data1";
let key = "?api_key=f6e3d483-2cab-438a-8b9e-d35082911743";
let i=0;
let arr = [];
let keysArr = ["name","address","admArea","district","typeObject","operatingCompany",
"publicPhone","rate","isNetObject","seatsCount","socialPrivileges","socialDiscount",
"set_1","set_2","set_3","set_4","set_5","set_6","set_7","set_8","set_9","set_10","id"]

function sendReq(url, method, onloadHandler, params) {
    console.log(url);
    let req = new XMLHttpRequest();
    req.open(method, url);
    req.responseType = 'json';
    req.onload = onloadHandler;
    req.send(params);
}

function outputRenderer(resp){
    document.getElementById("outputDiv").firstChild.remove();
    let elem = document.getElementById("outputDiv");
    let htmlin = `<b>${resp}</b>`;
    elem.append(htmlin);
}

function rI(min, max) {
    let rand = min + Math.random() * (max + 1 - min);
    return Math.floor(rand);
}

window.onload = function() {
    document.getElementById("cleanBtn").onclick = function() {
        console.log("cleanBtn");
        let buff = document.querySelectorAll("#form > div > div > input");
        for (let input of buff){
            input.value = null;
            console.log(input);
        }
    }
    document.getElementById("getBtn").onclick = function() {
        console.log("getBtn");
        url = host;
        console.log(url);
        sendReq(url, "GET", function () {
            console.log(this.response);
            outputRenderer(this.response);
        });
    }
    document.getElementById("searchBtn").onclick = function() {
        console.log("searchBtn");
        url = host+`/${document.getElementById("id").value}`;
        console.log(url);
        sendReq(url, "GET", function () {
            console.log(this.response);
            outputRenderer(this.response);
        });
    }
    document.getElementById("editBtn").onclick = function() {
        console.log("editBtn");
        url = host+`/${document.getElementById("id").value}`+key;
        console.log(url);
        let startObj = new FormData(document.getElementById("form"));
        let endObj = new FormData(); //didn't use the .delete() method because of browser compatibility
        for (let input of startObj){
            if (input[1] !== ""){
                endObj.append(input[0], input[1]);
                console.log("Appended!");
            }
        }
        for (let input of endObj){
            console.log(input);
        }
        sendReq(url, "PUT", function () {
            console.log(this.response);
            outputRenderer(this.response);
        }, endObj);
    }
    document.getElementById("addBtn").onclick = function() {
        console.log("addBtn");
        url = host+key;
        console.log(url);
        if (document.getElementById("randCheck").checked == true){
            let obj = new FormData();
            obj.append('address', `адрес ${rI(1,100000)} by LC`)
            obj.append('admArea', `округ ${rI(1,10)} by LC`)
            obj.append('district', `район ${rI(1,10)} by LC`)
            obj.append('isNetObject', rI(0,1))
            obj.append('name', `название ${rI(1,100000)} by LC`)
            obj.append('operatingCompany', `operComp ${rI(1,10)} by LC`)
            obj.append('publicPhone', `8 (800) 555-35-${rI(1,100000)}`)
            obj.append('rate', rI(1,99))
            obj.append('seatsCount', rI(1,500))
            obj.append('set_1', rI(1,1000))
            obj.append('set_2', rI(1,1000))
            obj.append('set_3', rI(1,1000))
            obj.append('set_4', rI(1,1000))
            obj.append('set_5', rI(1,1000))
            obj.append('set_6', rI(1,1000))
            obj.append('set_7', rI(1,1000))
            obj.append('set_8', rI(1,1000))
            obj.append('set_9', rI(1,1000))
            obj.append('set_10', rI(1,1000))
            obj.append('socialDiscount', rI(1,99))
            obj.append('socialPrivileges', rI(0,1))
            obj.append('typeObject', `тип ${rI(1,10)}`);

            sendReq(url, "POST", function () {
                console.log(this.response);
                outputRenderer(this.response);
            }, obj);
        } else {
            let obj = new FormData(document.getElementById("form"));
            sendReq(url, "POST", function () {
                console.log(this.response);
                console.log(obj);
                outputRenderer(this.response);
            }, obj);
        }
    }
    document.getElementById("deleteBtn").onclick = function() {
        console.log("deleteBtn");
        url = host+`/${document.getElementById("id").value}`+key;
        console.log(url);
        sendReq(url, "DELETE", function () {
            console.log(this.response);
            outputRenderer(this.response);
        });
    }
}